import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import VeeValidate from 'vee-validate';
import firebase from 'firebase/app';
import 'firebase/auth'
Vue.use(VeeValidate);

Vue.config.productionTip = false
let app;

firebase.auth().onAuthStateChanged((user)=> {


  if(!app) {
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount('#app')

  }

})
export const bus = new Vue();

