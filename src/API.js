import firebase from 'firebase/app';
import 'firebase/database';

var config = {
  apiKey: 'AIzaSyBpXQhvqk4md8m-QNzNwJEKRoiA_TNpGXU',
  authDomain: 'maretariumgame-3214.firebaseapp.com',
  databaseURL: 'https://maretariumgame-3214.firebaseio.com',
  projectId: 'maretariumgame-3214',
  storageBucket: 'maretariumgame-3214.appspot.com',
  messagingSenderId: '698445618179'
}

firebase.initializeApp(config)

export default {
  readQuestionData (poolcode, language) {
    let langpath ='';
    if(language == 'fin') {
      langpath = '/finnish_questions'
    } else {
      langpath = '/english_questions'
    }
    
    let questionIDs = []
    let questionData =[]
    let query = firebase.database().ref('/poolofquestions/' + poolcode).orderByKey();
     query.once('value').then((snapshot)=> {

      snapshot.forEach((childSnapshot)=>{
        questionIDs.push(childSnapshot.key)
      })

    });
    query = firebase.database().ref(langpath).orderByKey();

    return query.once('value').then((snapshot)=> {
      snapshot.forEach((childSnapshot)=> {
        if(questionIDs.includes(childSnapshot.key)) {
        let question = {
          "key" :childSnapshot.key,
          "data": childSnapshot.val() 
        }
        questionData.push(question);
        }
      })
    return questionData;
    })
  },

  removeQuestion (poolcode, questionId) {
    let query = firebase.database().ref('/poolofquestions/' + poolcode + '/' + questionId);
    query.remove();
    query = firebase.database().ref('/finnish_questions/' + questionId);
    query.remove();
    query = firebase.database().ref('/english_questions/' + questionId);
    query.remove();
  },
  addQuestionToPool(poolcode,newQuestionid) {

    firebase.database().ref('/english_questions/').child(newQuestionid).set(true)
    firebase.database().ref('/finnish_questions/').child(newQuestionid).set(true)
    return firebase.database().ref('/poolofquestions/' + poolcode).child(newQuestionid).set(true)
  },


  saveQuestion(language,questionID,question,ranswer,wanswer1,wanswer2,wanswer3) {
    
      let newQData = {
        Question : question,
        Ranswer: ranswer,
        Wanswer1 : wanswer1,
        Wanswer2 : wanswer2,
        Wanswer3: wanswer3
      }

      let langpath ='';
      if(language == 'fin') {
        langpath = '/finnish_questions/'
      } else {
        langpath = '/english_questions/'
      }
      let updates = {};
      updates[langpath + questionID + '/'] = newQData;

      return firebase.database().ref().update(updates);
  },
  fetchQuestionData(language,questionID) {
    let langpath ='';
    if(language == 'fin') {
      langpath = '/finnish_questions'
    } else {
      langpath = '/english_questions'
    }
    let query = firebase.database().ref(langpath +"/" + questionID).orderByKey();
    
    return query.once('value').then((snapshot)=> {
      return snapshot.val();
    });

  }
  

}
