import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Questions from './views/Questions.vue'
import About from './views/About.vue'
import LogIn from './components/LogIn.vue'
import firebase from 'firebase/app'
import 'firebase/auth'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home,
      meta:{
          requiresAuth:true
      }
    },
    {
      path : '*',
      redirect: '/login'
    },

    {
      path : '/',
      redirect: '/login'
    },
    
    {
      path: '/login',
      name: 'LogIn',
      component: LogIn
    },

    {
      path: '/about',
      name: 'about',
      component: About,

      meta:{
        requiresAuth:false
    },
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
    },
    {
      path: '/questions',
      name: 'questions',
      component: Questions,
      meta:{
        requiresAuth:true
    }
    }
  ]
})

router.beforeEach((to,from,next)=> {
  let currentUser = firebase.auth().currentUser;
  let requiresAuth = to.matched.some(record=>record.meta.requiresAuth); 

  if(requiresAuth && !currentUser) {
    next('login');
  } else if(!requiresAuth && currentUser) {
    next()
  } else {
    next();
  }

})

export default router;